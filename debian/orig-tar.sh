#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

DIR=jffi-$2

git clone http://github.com/jnr/jffi.git $DIR
cd $DIR
git checkout $2
cd ..
tar cfz ../jffi_$2.orig.tar.gz -X debian/orig-tar.exclude $DIR
rm -rf $DIR
cd ..
tar zxf jffi_$2.orig.tar.gz
